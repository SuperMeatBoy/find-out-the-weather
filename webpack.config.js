'use strict';
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports ={
    mode: 'development',
    resolve: {
        alias: {
            containers: path.resolve(__dirname, 'src/containers/'),
            components: path.resolve(__dirname, 'src/components/'),
            utils: path.resolve(__dirname, 'src/utils/')
        }
    },
    //entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        compress: false,
        port: 9000,
        historyApiFallback: {
            disableDotRule: true
        }
    },
    module: {
        rules: [
            {
                test: /\.(png|jpeg|jpg|gif|ico|svg)$/i,
                loader: 'file-loader',
                options: {
                    outputPath: 'images',
                    name: '[name]-[sha1:hash:7].[ext]'
                },
            },
            {
                test: /\.mp4$/i,
                loader: 'file-loader',
                options: {
                    outputPath: 'video',
                },
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env']
                    }
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.s[ca]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "public/index.html"
        })
    ],
}