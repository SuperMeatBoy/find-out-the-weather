import React from 'react'
import {Table} from 'react-bootstrap'
import {Link} from "react-router-dom";

const TableCity = ({list}) => {

    if(!list.length) return false
    return (
        <Table responsive>
            <thead>
                <tr>
                    <th>City</th>
                    <th>Temperature (°С)</th>
                    <th>Wind (m/s)</th>
                    <th>Clouds (%)</th>
                    <th>Pressure (hpa)</th>
                </tr>
            </thead>
            <tbody>
                {list.map(i => {
                    if(!!i) return (
                        <tr key={i.id}>
                            {i.coord && <td>
                                <Link to={`city/${i.name}/${i.coord.lat}_${i.coord.lon}`}><b> {i.name} ({i.sys.country}) </b></Link>
                                <div>Geo coords: [{i.coord.lat}, {i.coord.lon}]</div>
                            </td>}
                            <td>from <b>{i.main.temp_min}</b> to <b>{i.main.temp_max}</b></td>
                            <td>{i.wind.speed}</td>
                            <td>{i.clouds.all}</td>
                            <td>{i.main.pressure}</td>
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    )
}

export default TableCity