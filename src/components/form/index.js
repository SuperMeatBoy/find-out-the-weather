import React, {useRef, useState} from "react";
import {Form, Button} from 'react-bootstrap'
import { fetchCities } from 'containers/slices'
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";

const CityForm = props => {
    const dispatch = useDispatch()
    let history = useHistory()
    const [validated, setValidated] = useState(false);
    const [city, setCity] = useState('');
    const formRef = useRef(null);

    const handleReset = () => {
        formRef.current.reset();
        setCity('')
        setValidated(false);
        history.push('/')
    };

    const handleSubmit = e => {
        e.preventDefault();
        if (e.currentTarget.checkValidity() === false) {
            e.stopPropagation();
        }
        setValidated(true);
        if(formRef.current['0'].value.length){
            dispatch(fetchCities(city))
            handleReset()
        }
    }

    return (
        <Form className='form' noValidate ref={formRef} validated={validated} onSubmit={handleSubmit}>
            <Form.Group className='mr-2'>
                <Form.Control type="text" placeholder="City" value={city} onChange={e => setCity(e.currentTarget.value)} required />
                <Form.Control.Feedback type="invalid">
                    Please provide a valid city.
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
                <Button type="submit">Search</Button>
            </Form.Group>
        </Form>
    )
}
export default CityForm
