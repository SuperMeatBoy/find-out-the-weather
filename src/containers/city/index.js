import React, {useEffect} from "react";
import {fetchCity} from "containers/slices";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router";
import {Table} from "react-bootstrap";
import {formattedDate} from "utils/moment";
import { format, compareAsc } from 'date-fns'

const City = props => {
    const dispatch = useDispatch()
    const {id, name} = useParams();
    const {city} = useSelector(reducer => reducer.cities)
    const date = new Date().toLocaleString()

    useEffect(() => {
        if (id) {
            const [lat, lon] = id.split('_')
            dispatch(fetchCity(lat, lon))
        }
    }, [id, dispatch])

    if (!Object.keys(city).length) return null
    return (
        <div className="container pt-4">
            <h2 className='text-center mb-3'>Current weather and forecasts in your city</h2>
            <div className='city-title mb-3'>
                <h5 className='mb-0'>Weather in {name} | {Math.ceil(city.current.temp)} °С</h5>
                <div className="time">{date}</div>
            </div>
            <Table className='city-table mb-5' striped bordered hover size="sm">
                <tbody>
                <tr>
                    <td>Wind</td>
                    <td>{city.current.wind_speed} m/s</td>
                </tr>
                <tr>
                    <td>Cloudiness</td>
                    <td>{city.current.weather[0].description}</td>
                </tr>
                <tr>
                    <td>Pressure</td>
                    <td>{city.current.pressure} hpa</td>
                </tr>
                <tr>
                    <td>Humidity</td>
                    <td>{city.current.humidity} %</td>
                </tr>
                <tr>
                    <td>Timezone</td>
                    <td>{city.timezone}</td>
                </tr>
                <tr>
                    <td>Geo coords</td>
                    <td>[{city.lat}, {city.lon}]</td>
                </tr>
                </tbody>
            </Table>

            <h5>7 days weather forecast</h5>
            <Table responsive>
                <thead>
                <tr>
                    <th>Day</th>
                    <th>Temperature (°С)</th>
                    <th>Wind (m/s)</th>
                    <th>Clouds (%)</th>
                    <th>Pressure (hpa)</th>
                </tr>
                </thead>
                <tbody>
                {city.daily.map(i => {
                    if(!!i) return (
                        <tr key={i.dt.toLocaleString()}>
                            <td>
                                {format(new Date(i.dt * 1000), 'iiii MM/dd/yyyy')}
                            </td>
                            <td>from <b>{i.temp.min.toFixed(0)}</b> to <b>{i.temp.max.toFixed(0)}</b></td>
                            <td>{i.wind_speed}</td>
                            <td>{i.clouds}</td>
                            <td>{i.pressure}</td>
                        </tr>
                    )
                })}
                </tbody>
            </Table>
        </div>
    )
}

export default City