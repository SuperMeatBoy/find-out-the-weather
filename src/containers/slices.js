import { createSlice } from '@reduxjs/toolkit'
import { apiKey } from './data'

export const citySlice = createSlice({
    name: 'city',
    initialState: {
        isLoading: false,
        citiesHistory: [],
        searchResult: [],
        city: {},
    },
    reducers: {
        getCitiesStart: state => {
            state.isLoading = true
        },
        getCitiesSuccess: (state, {payload}) => {
            state.isLoading = false

            if(!!payload.list) {
                state.citiesHistory = state.citiesHistory.concat(payload.list)
                if(state.citiesHistory.length > 5) state.citiesHistory.splice(0, state.citiesHistory.length - 5)
                state.searchResult = payload.list
            }
        },
        getCitiesError: state => {
            state.isLoading = false
        },

        getCityStart: state => {
            state.isLoading = true
        },
        getCitySuccess: (state, {payload}) => {
            state.isLoading = false
            if (!!payload) state.city = payload
        },
        getCityError: state => {
            state.isLoading = false
        },
        cityCleaner: state => {
            state.searchResult = []
        },
    },
})

export const {
    getCitiesStart,
    getCitiesSuccess,
    getCitiesError,
    getCityStart,
    getCitySuccess,
    getCityError,
    cityCleaner,
} = citySlice.actions

export const { reducer } = citySlice

export const fetchCities = city => dispatch => {
    try {
        dispatch(getCitiesStart())
        fetch(`https://api.openweathermap.org/data/2.5/find?q=${city}&appid=${apiKey}&units=metric`)
            .then(response => response.json())
            .then(json => dispatch(getCitiesSuccess(json)))
    } catch (err) {
        dispatch(getCitiesError(err))
    }
}
export const fetchCity = (lat, lon) => dispatch => {
    try {
        dispatch(getCityStart())
        fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`)
            .then(response => response.json())
            .then(json => dispatch(getCitySuccess(json)))
    } catch (err) {
        dispatch(getCityError(err))
    }
}