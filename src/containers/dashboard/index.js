import React, {useEffect} from 'react'
import TableCity from "components/table";
import {useDispatch, useSelector} from "react-redux";
import {Alert} from "react-bootstrap";
import { cityCleaner } from "containers/slices";

export const Dashboard = props => {
    const { searchResult, citiesHistory } = useSelector(reducer => reducer.cities)
    const dispatch = useDispatch()

    useEffect(() => {
        return () => {
            dispatch(cityCleaner())
        }
    },[dispatch])

    if(!searchResult.length && !citiesHistory.length) return (
        <div className="container pt-5">
            <h2 className='text-center'>Nothing here yet, make your first request</h2>
        </div>
    )
    if(!searchResult.length) return (
        <section className="container-fluid">
            <div className="container pt-4">
                <h2 className='text-center mb-4'>You searched recently</h2>
                <TableCity list={citiesHistory} />
            </div>
        </section>
    )
    return (
        <section className="container-fluid">
            <div className="container">
                <TableCity list={searchResult} />
            </div>
        </section>
    )
}

export default Dashboard