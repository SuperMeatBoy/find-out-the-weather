import moment from 'moment'
import momentTimeZones from 'moment-timezone'

export const timeZones = momentTimeZones.tz.names()
export const timeZoneLabel = i => `(GMT ${momentTimeZones.tz(i).format('Z')}) ${i}`

export function formattedDate(date, format = 'YYYY-MM-DD h:mm:ss') {
	return moment(date).format(format)
}
