import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'

import { reducer as citiesReducer } from 'containers/slices'

// const preloadedState = localStorage.getItem('preloadedState')
//     ? JSON.parse(localStorage.getItem('preloadedState'))
//     : {}
const preloadedState = {}

const store = configureStore({
    reducer: {
        cities: citiesReducer
    },
    middleware: [...getDefaultMiddleware()],
    preloadedState
})

// store.subscribe(() =>
//     localStorage.setItem('preloadedState', JSON.stringify(store.getState()))
// )

export default store