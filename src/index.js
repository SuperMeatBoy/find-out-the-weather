import React from 'react'
import {render} from 'react-dom'
import App from './app'
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/index.scss'

render(<App />, document.getElementById('root'))