import React from 'react'
import { Provider } from 'react-redux'
import store from './store'
import { Redirect, BrowserRouter, Route, Switch } from "react-router-dom"
import Dashboard from "./containers/dashboard"
import City from "./containers/city"
import CityForm from "components/form"

export default () => {
    return (
        <BrowserRouter>
            <Provider store={store}>
                <section className="container-fluid">
                    <div className='video'>
                        <video src="" autoPlay muted loop/>
                    </div>
                    <div className="container">
                        <CityForm />
                    </div>
                </section>
                <Switch>
                    <Route exact path='/' component={Dashboard}/>
                    <Route path='/city/:name/:id' component={City}/>
                    <Redirect to='/'/>
                </Switch>
            </Provider>
        </BrowserRouter>
    )
}